cmake_minimum_required(VERSION 3.5)

project(geographiclib_vendor)

find_package(ament_cmake REQUIRED)

set(PACKAGE_VERSION "1.0.0")

macro(build_geographiclib)
  set(configure_opts)

  if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    set(TARGET "--host=x86_64-pc-nto-qnx7.1.0")
  elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "aarch64le")
    set(TARGET "--host=aarch64-unknown-nto-qnx7.1.0")
  else()
    message(FATAL_ERROR "invalid cpu CMAKE_SYSTEM_PROCESSOR:${CMAKE_SYSTEM_PROCESSOR}")
  endif()

  list(APPEND configure_opts "${TARGET}")
  list(APPEND configure_opts "--prefix=${CMAKE_INSTALL_PREFIX}")
  list(APPEND configure_opts "--with-sysroot=${CMAKE_INSTALL_PREFIX}")
  list(APPEND configure_opts "CPPFLAGS=${CPPFLAGS} -D_QNX_SOURCE")

  set(PATCHFILE "${CMAKE_CURRENT_SOURCE_DIR}/geographiclib.patch")
  include(ExternalProject)
  ExternalProject_Add(geographiclib
    GIT_REPOSITORY https://github.com/geographiclib/geographiclib
    GIT_TAG 560bfd94c71b23438988df6eab13e121f8cb0f96
    TIMEOUT 600
    PATCH_COMMAND git apply --ignore-space-change --ignore-whitespace ${PATCHFILE}
    || git apply --ignore-space-change --ignore-whitespace ${PATCHFILE}  -R --check && echo "geographiclib_vendor patch already applied"
    CONFIGURE_COMMAND cd ${CMAKE_CURRENT_BINARY_DIR}/geographiclib-prefix/src/geographiclib && ./configure "${configure_opts}"
    BUILD_COMMAND     cd ${CMAKE_CURRENT_BINARY_DIR}/geographiclib-prefix/src/geographiclib && make
    INSTALL_COMMAND   cd ${CMAKE_CURRENT_BINARY_DIR}/geographiclib-prefix/src/geographiclib && make install
  )

endmacro()

find_package(geographiclib QUIET)
if(NOT geographiclib_FOUND)
  build_geographiclib()

  if(WIN32)
    ament_environment_hooks(env_hook/geographiclib_vendor_library_path.bat)
    set(ENV_VAR_NAME "PATH")
    set(ENV_VAR_VALUE "opt\\geographiclib_vendor\\bin")
  else()
    ament_environment_hooks(env_hook/geographiclib_vendor_library_path.sh)
    if(APPLE)
      set(ENV_VAR_NAME "DYLD_LIBRARY_PATH")
    else()
      set(ENV_VAR_NAME "LD_LIBRARY_PATH")
    endif()
    set(ENV_VAR_VALUE "opt/geographiclib_vendor/lib")
  endif()
  ament_environment_hooks(env_hook/geographiclib_vendor_library_path.dsv.in)
else()
  message(STATUS "Found geographiclib ${geographiclib_VERSION} in path ${geographiclib_CONFIG}")
endif()

ament_package()
